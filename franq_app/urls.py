"""franq_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.shortcuts import redirect

from person import views as person_views
from garage import views as garage_views

urlpatterns = [
    path('', lambda req: redirect('/person/')),
    path('person/', include('person.urls')),
    path('person/', include('django.contrib.auth.urls')),
    path('garage/', include('garage.urls')),
    path('vehicle/', include('vehicle.urls')),
    path('thirdparty/clients', person_views.get_all_persons),
    path('thirdparty/garages', garage_views.list_active_garages),
    path('thirdparty/clientsgarage', person_views.get_parking_user_info),
    path('admin/', admin.site.urls),
]
