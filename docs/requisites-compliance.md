# Technical Test Compliance

* *Crie uma aplicação django que guarde os dados cadastrais de uma pessoa;
não precisa realizar um cadastro completo, apenas nome, telefone e e-mail já é
suficiente. Para facilitar o entendimento, vamos chamar essa aplicação de
Pessoa.*

**The application contains its root config folder at [franq_app](../franq_app) folder.
The application extends original django `User` model with a `Person` model (profile)
and add the necessary fields, i.e phone. The other fields are already present
at `User` model.**

* *Crie uma segunda aplicação django para gerenciar garagens de veículos e
seus respectivos veículos. Vamos chamar essa aplicação de Garagem.*

**The application was created under [garage](../garage) folder.**

* *Crie dois tipos de pessoas, uma é o consumidor, e outra é o administrador do
sistema e defina seus respectivos escopos de acessos para ambas aplicações.*j

**The costumer user has its access limited to his own garage and profile. Admin
users leverages `admin` features and `is_superuser` from `User` model field to
obtain a global control.**

* *Construa uma API entre as duas aplicações, onde para cada pessoa
cadastrada seja criada uma garagem para ela na segunda aplicação
(Garagem)*

**The Garage [model](../garage/models.py) has an OneToOne Link to User and 
leverages django's [signals](https://docs.djangoproject.com/en/3.1/topics/signals/)
to create a Garage model for each User.**

* *Na aplicação Garagem forneça ao usuário a lista de veículos para que ele
possa vincular a sua garagem e em seguida faça um endpoint para exibir todos
os veículos que esse respectivo usuário escolheu para deixar em sua garagem.*

**The user could see the available vehicles at `/garage/available` route and its
garage vehicles at `/garage` route.**

**The user can add/remove vehicles into its garage by using the UI or through
`/vehicle/<id>/garage/add`(Insert vehicle) and `/vehicle/<id>/garage/remove`(Remove vehicle)
endpoints. Those actions are restricted only to available vehicles and 
user's own garage.**

* *Para cada veículo crie um método que retorne a sua cor e seu ano de
fabricação, porém, se o veículo for uma moto, deve retornar o modelo no lugar
da cor junto com o ano de fabricação.*

**The vehicle tailored info can be seen at `/vehicle/<id>/info` endpoint at JSON
format.**

* *Apenas os dados de e-mail e telefônico podem ser persistidos na aplicação
garagem sendo assim, todos os demais dados devem permanecer na
aplicação Pessoa.*

**Per Garage [model](../garage/models.py), it contains only User's foreign key,
email and phone.**

* *Faça uso das boas práticas de desenvolvimento e segurança de API’s e bem
como para login dos usuários.*

**The application leverages Django admin by restricting admin accesses and action
through `/admin` routes and normal users through `/person` routes**

* *Por fim crie uma API para uma aplicação terceira consultar todos os clientes
cadastrados, todas as garagens ativas, e quais clientes possuem veículos
vinculados a suas garagens e quais não possuem.*

**The application provide those API's under `/thirdparty` route:**

* **`/thirdparty/clients` - All registered clients**
* **`/thirdparty/garages` - All active garages**
* **`/thirdparty/clientsgarage` - User who do/do not have vehicles into his garage.**

