from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Garage(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.CharField(max_length=255, null=True, blank=True)
    phone = models.CharField(max_length=255, null=True, blank=True)

@receiver(post_save, sender=User)
def create_user_garage(sender, instance, created, **kwargs):
    if created:
        Garage.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_garage(sender, instance, **kwargs):
    instance.garage.save()
