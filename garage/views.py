from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

from vehicle.models import Vehicle
from garage.models import Garage

import json

@login_required
def available_list(request):
    available_vehicles = Vehicle.available_vehicles()
    return render(request,'garage/available.html',{ 'available_vehicles': available_vehicles })

@login_required
def index(request):
    garage_vehicles = Vehicle.garage_vehicles(request.user.garage)
    return render(request,'garage/index.html',{ 'garage_vehicles': garage_vehicles })

#TODO: FT-32 - Add is_active field into Garage
def list_active_garages(request):
    response_data = []
    for garage in Garage.objects.all() :
        response_data.append({'garage_id':garage.id, 'user_fullname':garage.user.get_full_name()})
    return HttpResponse(json.dumps({ 'garages': response_data }), content_type="application/json")
