from django.urls import path

from . import views

app_name = "person"

urlpatterns = [
    path('', views.index, name='index'),
    path('update', views.update_person, name='update'),
    path('signup', views.signup_person, name='signup'),
]
