from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from person.models import Person
from garage.models import Garage

class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')

class UserCreateForm(UserCreationForm):
    email = forms.EmailField(max_length=150, required=True)
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'first_name', 'last_name',)

class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ("phone",)

class GarageForm(forms.ModelForm):
    class Meta:
        model = Garage
        fields = ("phone","email",)
