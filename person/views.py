from django.db import transaction
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.translation import gettext as _

from .forms import UserUpdateForm, UserCreateForm, PersonForm, GarageForm
import json

@login_required
def index(request):
    user_form = UserUpdateForm(instance=request.user)
    person_form = PersonForm(instance=request.user.person)
    return render(request,'person/index.html',{'user_form':user_form,'person_form':person_form})

@login_required
@transaction.atomic
def update_person(request):
    if request.method == 'POST':
        #TODO: Check better way to cascade update related tables
        user_form = UserUpdateForm(request.POST, instance=request.user)
        person_form = PersonForm(request.POST, instance=request.user.person)
        garage_form = GarageForm(request.POST, instance=request.user.garage)
        if user_form.is_valid() and person_form.is_valid():
            user_form.save()
            person_form.save()
            garage_form.save()
            messages.success(request, _('Your profile was successfully updated!'))
            return redirect('person:index')
        else:
            messages.error(request, _('please correct the error below.'))

def signup_person(request):
    if request.method == 'POST':
        user_form = UserCreateForm(request.POST)
        person_form = PersonForm(request.POST)
        if user_form.is_valid() and person_form.is_valid():
            user = user_form.save()
            user.refresh_from_db()
            user.person.phone = person_form.cleaned_data.get('phone')
            user.garage.phone = person_form.cleaned_data.get('phone')
            user.garage.email = user_form.cleaned_data.get('email')
            user.save()
            username = user_form.cleaned_data.get('username')
            password = user_form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, _('Your profile was successfully created!'))
            return redirect('person:index')
        else:
            messages.error(request, _('please correct the error below.'))
            return render(request,'registration/signup.html',{'user_form':user_form,'person_form':person_form})
    else:
        user_form = UserCreateForm()
        person_form = PersonForm()
        return render(request,'registration/signup.html',{'user_form':user_form,'person_form':person_form})

def get_all_persons(request):
    response_data = []
    for user in User.objects.all() :
        response_data.append({'user_id':user.id, 'fullname':user.get_full_name()})
    return HttpResponse(json.dumps({ 'users': response_data }), content_type="application/json")

#TODO: Add serializers for better handling with conversion
def get_parking_user_info(request):
    response_data = {
        'parked_users': [],
        'empty_users': []
    }
    parked_users = User.objects.exclude(garage__vehicle__garage_id=None)
    empty_users = User.objects.filter(garage__vehicle__garage_id=None)
    for user in parked_users:
        response_data['parked_users'].append({'user_id':user.id, 'fullname':user.get_full_name()})
    for user in empty_users:
        response_data['empty_users'].append({'user_id':user.id, 'fullname':user.get_full_name()})
    return HttpResponse(json.dumps( response_data ), content_type="application/json")
