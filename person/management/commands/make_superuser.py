from django.core.management.base import BaseCommand
from django.contrib.auth.models import User;
import os

class Command(BaseCommand):

    def handle(self, *args, **options):
        admin_user = os.environ.get("SUPERUSER_USERNAME", default='admin')
        email = os.environ.get("SUPERUSER_EMAIL", default='admin@domain.com')
        password = os.environ.get("SUPERUSER_PASSWORD", default='admin')

        admin = User.objects.create_superuser(username=admin_user,
                    email=email,
                    password=password)
        admin.is_active = True
        admin.is_admin = True
        admin.save()
