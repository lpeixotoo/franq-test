Franq Test App
--------------

# Overview

Franq Test App is an implementation project of Franq Openbank software engineer's
techinical test. The project allows you to manage your own garage by adding and
removing listed vehicles.

The project allows its users to:

* Mantain application users, garages and vehicles (Admin Only);
* Signup/Login;
* Add/Remove available cars into your garages;
* Get all application users; 
* Get all application garages; 
* Get all users whose garages has/has not vehicles;

# Getting started

## How to run

### Docker

The application requires `docker-compose` to be installed along with `Docker`.

To build and run the application execute the following command:

```
docker-compose up --build
```

**Notes**: `-d` option could be used if a run as a daemon is preffered.

### Checking out Franq Test App

In order to access the application type access the following URL in your browser:

`http://localhost:8000`

# Further Documentation

* **Requisites Compliance **: A quick guide on how the project address the requisites, [here](./docs/requisites-compliance.md).
* **Test Requisites**: The whole test requisites could be found [here](./docs/requisites.md).

