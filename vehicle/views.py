from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.decorators import login_required

from vehicle.models import Vehicle
import json

@login_required
def add_to_garage(request, vehicle_id):
    selected_vehicle = get_object_or_404(Vehicle, pk=vehicle_id)
    selected_vehicle.garage = request.user.garage
    selected_vehicle.save()
    #TODO: Change Redirect (Have list and garage in one place)
    return HttpResponseRedirect(reverse('garage:index'))

@login_required
def remove_from_garage(request, vehicle_id):
    selected_vehicle = get_object_or_404(Vehicle, pk=vehicle_id)
    if selected_vehicle.garage == request.user.garage:
        selected_vehicle.garage = None
        selected_vehicle.save()
    #TODO: Change Redirect (Have list and garage in one place)
    return HttpResponseRedirect(reverse('garage:index'))

def info(request, vehicle_id):
    selected_vehicle = get_object_or_404(Vehicle, pk=vehicle_id)
    response_data = {}
    if selected_vehicle.v_type == 'car':
        response_data['color'] = selected_vehicle.color
    else:
        response_data['model'] = selected_vehicle.model
    response_data['manufactured_year'] = selected_vehicle.manufactured_year
    return HttpResponse(json.dumps(response_data), content_type="application/json")
