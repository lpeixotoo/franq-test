from django.urls import path

from . import views

app_name = "vehicle"

urlpatterns = [
    path('<int:vehicle_id>/garage/add', views.add_to_garage, name='addgarage'),
    path('<int:vehicle_id>/garage/remove', views.remove_from_garage, name='removegarage'),
    path('<int:vehicle_id>/info', views.info, name='info'),
]
