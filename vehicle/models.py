from django.db import models
from garage.models import Garage

class Vehicle(models.Model):

    WHITE="white"
    BLACK="black"
    RED="red"
    GRAY="gray"
    BLUE="blue"

    COLOR_CHOICES = [
        (WHITE, "White"), 
        (BLACK, "Black"), 
        (RED, "Red"),
        (GRAY, "Gray"),
        (BLUE, "Blue"),
    ]

    FIAT="fiat"
    GM="gm"
    VW="vw"
    HONDA="honda"
    DUCATI="ducati"
    HYUNDAI="hyundai"

    BRAND_CHOICES = [
        (FIAT, "Fiat"), 
        (GM, "GM"), 
        (VW, "VW"),
        (HONDA, "Honda"),
        (DUCATI, "Ducati"),
        (HYUNDAI, "Hyundai"),
    ]

    CAR="car"
    MOTORCYCLE="motorcycle"

    VEHICLE_TYPE_CHOICES = [
        (CAR, "Car"), 
        (MOTORCYCLE, "Motorcycle"), 
    ]

    # Garage key will be set to NULL if garage has been deleted
    garage = models.ForeignKey(Garage, blank=True, null=True,
            on_delete=models.SET_NULL)
    model = models.CharField(max_length=155)
    color = models.CharField(max_length=10, choices=COLOR_CHOICES)
    brand = models.CharField(max_length=10, choices=BRAND_CHOICES)
    v_type = models.CharField(max_length=10, choices=VEHICLE_TYPE_CHOICES)
    manufactured_year = models.IntegerField()

    def available_vehicles():
        return Vehicle.objects.filter(garage=None).values()

    def garage_vehicles(garage):
        return Vehicle.objects.filter(garage=garage).values()
